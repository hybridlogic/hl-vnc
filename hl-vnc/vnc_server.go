package main

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"github.com/kbinani/screenshot"
	"github.com/mitchellh/go-vnc"
	"image"
	"log"
	"net"
)

func main() {
	ln, err := net.Listen("tcp", ":5901")
	if err != nil {
		// handle error
	}
	log.Println("Starting Server")
	for {
		conn, err := ln.Accept()
		if err != nil {
			// handle error
		}
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	defer conn.Close()
	log.Println("Remote: ", conn.RemoteAddr())
	conn.Write([]byte("RFB 003.008\n"))

	reader := bufio.NewReader(conn)

	// VERSION HANDSHAKE
	data, _, _ := reader.ReadLine()
	version := string(data)
	//if version != "RFB 003.008\n" {
	//	log.Fatalf("Server does not understand version:", version)
	//}
	log.Println("Using version number:", version)

	// SECURITY HANDSHAKE
	var secBuf bytes.Buffer
	var security uint8 = 1
	if err := binary.Write(&secBuf, binary.BigEndian, security); err != nil {
		log.Fatalln(err)
	}

	securityTypes := [1]uint8{1}
	if err := binary.Write(&secBuf, binary.BigEndian, &securityTypes); err != nil {
		log.Fatalln(err)
	}
	if _, err := conn.Write(secBuf.Bytes()); err != nil {
		log.Fatalln(err)
	}

	var clientType uint8
	if err := binary.Read(conn, binary.BigEndian, &clientType); err != nil {
		log.Fatalln(err)
	}
	log.Println("Security Type Selected:", clientType)

	////read a failure?
	//var securityError uint32
	//if err := binary.Read(conn, binary.BigEndian, &securityError); err != nil {
	//	log.Fatalln(err)
	//}
	//log.Println("Error?", securityError)

	var good uint32 = 0
	if err := binary.Write(conn, binary.BigEndian, good); err != nil {
		log.Fatalln(err)
	}

	//7.3 ClientInt
	var clientInit uint8
	if err := binary.Read(conn, binary.BigEndian, &clientInit); err != nil {
		log.Fatalln(err)
	}
	log.Println("What to do with other clients:", clientInit)

	// 7.4 ServerInit
	var initBuf bytes.Buffer
	bounds := getBounds()
	log.Println(bounds, bounds.Min, bounds.Max)
	log.Println("Sending ServerInit message")
	var fbWidth = uint16(bounds.Max.X) //= 1024
	if err := binary.Write(&initBuf, binary.BigEndian, fbWidth); err != nil {
		log.Fatalln(err)
	}
	var fbHeight = uint16(bounds.Max.Y) //768
	if err := binary.Write(&initBuf, binary.BigEndian, fbHeight); err != nil {
		log.Fatalln(err)
	}

	pixelFormat := vnc.PixelFormat{
		BPP:        24,
		Depth:      24,
		BigEndian:  true,
		TrueColor:  true,
		RedMax:     255,
		GreenMax:   255,
		BlueMax:    255,
		RedShift:   0,
		GreenShift: 8,
		BlueShift:  16,
	}
	//var fbFormat uint16 =
	pixels, _ := writePixelFormat(&pixelFormat)
	if err := binary.Write(&initBuf, binary.BigEndian, pixels); err != nil {
		log.Fatalln(err)
	}

	// Name
	var fbNameLength uint32 = 2
	if err := binary.Write(&initBuf, binary.BigEndian, fbNameLength); err != nil {
		log.Fatalln(err)
	}
	var fbName = [2]uint8{67, 64}
	if err := binary.Write(&initBuf, binary.BigEndian, fbName); err != nil {
		log.Fatalln(err)
	}
	if _, err := conn.Write(initBuf.Bytes()); err != nil {
		log.Fatalln(err)
	}


	mainLoop(conn)

	log.Println("Bye Bye")

	////var data []byte
	//for {
	//	var clientType uint8
	//	if err := binary.Read(conn, binary.BigEndian, &clientType); err != nil {
	//		log.Fatalln(err)
	//	}
	//	log.Println("Security Type Selected:", clientType)
	//}
}

func mainLoop(conn net.Conn) {
	T := NewTimer()

	// Build the map of available server messages
	typeMap := make(map[uint8]ClientMessage)

	defaultMessages := []ClientMessage{
		new(SetPixelFormat),
		new(SetEncodings),
		new(FramebufferUpdateRequest),
		new(KeyEvent),
		new(PointerEvent),
		new(ClientCutText),
	}

	for _, msg := range defaultMessages {
		typeMap[msg.Type()] = msg
	}

	//if c.config.ServerMessages != nil {
	//	for _, msg := range c.config.ServerMessages {
	//		typeMap[msg.Type()] = msg
	//	}
	//}

	bounds := getBounds()     // bounds don't change?
	rgba := getScreen(bounds) // first image

	for {
		var messageType uint8
		if err := binary.Read(conn, binary.BigEndian, &messageType); err != nil {
			//break
			log.Fatalln(err)
		}

		log.Println("Type:", messageType)

		msg, ok := typeMap[messageType]
		if !ok {
			// Unsupported message type! Bad!
			//break
			log.Fatalln("Unsupported message type! Bad!", messageType)
		}

		parsedMsg, err := msg.Read(conn)
		if err != nil {
			//break
			log.Fatalln(err)
		}

		//if c.config.ServerMessageCh == nil {
		//	continue
		//}

		//c.config.ServerMessageCh <- parsedMsg
		//log.Println("  Message:", parsedMsg)
		if parsedMsg.Type() == 3 {
			log.Println("Need to send a framebuffer update.")
			//todo: get screenshot, figure out things

			rgbaNew := getScreen(bounds)

			T.Start("Diff Image")
			//rgba = imageDiffs(rgba, rgbaNew)
			rects := imageDiffs(rgba, rgbaNew)
			log.Println("Diff Image:", T.End("Diff Image"))
			bounds = image.Rect(0, 0, bounds.Max.X, bounds.Max.Y)
			fum := FramebufferUpdateMessage{
				Rectangles:rects,
				//Rectangles: []*Rectangle{{
				//	X:      uint16(bounds.Min.X),
				//	Y:      uint16(bounds.Min.Y),
				//	Height: uint16(bounds.Max.Y),
				//	Width:  uint16(bounds.Max.X),
				//	Image:  rgba,
				//}},
			}

			T.Start("Sending Image")
			fum.Write(conn)
			log.Println("Sending Image", T.End("Sending Image"))

			rgba = rgbaNew
			//time.Sleep(time.Second)
		}
	}
}

func getScreen(bounds image.Rectangle) *image.RGBA {
	n := screenshot.NumActiveDisplays()
	if n <= 0 {
		panic("Active display not found")
	}
	//bounds := image.Rect(0,0,1024, 768)
	img, err := screenshot.CaptureRect(bounds)
	if err != nil {
		panic(err)
	}
	return img
}

func getBounds() image.Rectangle {
	n := screenshot.NumActiveDisplays()
	if n <= 0 {
		panic("Active display not found")
	}
	for i := 0; i < n; i++ {
		log.Println(i, screenshot.GetDisplayBounds(i))
	}
	bounds := screenshot.GetDisplayBounds(0)
	return bounds
}
