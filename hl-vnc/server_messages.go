package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"image"
	"io"
	"log"
)

// A ServerMessage implements a message sent from the server to the client.
type ServerMessage interface {
	// The type of the message that is sent down on the wire.
	Type() uint8

	// Read reads the contents of the message from the reader. At the point
	// this is called, the message type has already been read from the reader.
	// This should return a new ServerMessage that is the appropriate type.
	Write(io.Writer) (ServerMessage, error)
}

// FramebufferUpdateMessage consists of a sequence of rectangles of
// pixel data that the client should put into its framebuffer.
type FramebufferUpdateMessage struct {
	Rectangles []*Rectangle
}

// Rectangle represents a rectangle of pixel data.
type Rectangle struct {
	X      uint16
	Y      uint16
	Width  uint16
	Height uint16
	Enc    Encoding
	Image  *image.RGBA
	I      *image.Image
}

func (rec *Rectangle) String() string {
	return fmt.Sprintf("X:%v, Y:%v, Width:%v, Height:%v, ",
		rec.X, rec.Y, rec.Width, rec.Height)
}

func (rec *Rectangle) Write(buf *bytes.Buffer) error {
	//if rec.X == rec.Y {
	//	log.Println("Rec Stuff", rec.X, rec.Y)
	//}

	// RECTANGLE Heading
	//x position
	if err := binary.Write(buf, binary.BigEndian, rec.X); err != nil {
		return err
	}
	//y position
	if err := binary.Write(buf, binary.BigEndian, rec.Y); err != nil {
		return err
	}
	//width
	if err := binary.Write(buf, binary.BigEndian, rec.Width); err != nil {
		return err
	}
	// height
	if err := binary.Write(buf, binary.BigEndian, rec.Height); err != nil {
		return err
	}
	//encoding-type
	if err := binary.Write(buf, binary.BigEndian, int32(0)); err != nil {
		return err
	}

	// RECTANGLE DATA
	//bpp := uint8(32 / 8)
	//pixelData := [4]byte{0,200,0,0}
	//for y := uint16(0); y < rec.Height; y++ {
	//	for x := uint16(0); x < rec.Width; x++ {
	//log.Println("Image Rectable:",rec.Image.Rect.Min.X, rec.Image.Rect.Max.X)
	for y := rec.Image.Rect.Min.Y; y <= rec.Image.Rect.Max.Y; y++ {
		for x := rec.Image.Rect.Min.X; x <= rec.Image.Rect.Max.X; x++ {
			//r, g, b, a := rec.Image.At(int(x), int(y)).RGBA()
			//if y == x {
			//	log.Println("Pixel Data", rec.)
			//}
			//binary.Write(&buf, binary.BigEndian, pix.RGBA())
			i := rec.Image.RGBAAt(int(x), int(y))
			pixelData := [4]byte{i.B, i.G, i.R, i.A} // reverse for LittleEndian
			//pixelData := [4]byte{uint8(r), uint8(g), uint8(b), uint8(a)}
			for _, b := range pixelData {
				binary.Write(buf, binary.BigEndian, b)
			}
		}
	}
	return nil
}

func (fum *FramebufferUpdateMessage) Type() uint8 {
	return 0
}

func (fum *FramebufferUpdateMessage) Write(w io.Writer) (ServerMessage, error) {
	var buf bytes.Buffer
	// type: 0
	// todo: move to outside the function? like the reads?
	if err := binary.Write(&buf, binary.BigEndian, fum.Type()); err != nil {
		return nil, err
	}
	// padding
	if err := binary.Write(&buf, binary.BigEndian, uint8(0)); err != nil {
		return nil, err
	}
	// number of rectangles
	if err := binary.Write(&buf, binary.BigEndian, uint16(len(fum.Rectangles))); err != nil {
		return nil, err
	}
	log.Println("Sending number of Rects:", len(fum.Rectangles))
	for _, rectangle := range fum.Rectangles {
		//log.Println(rectangle)
		if err := rectangle.Write(&buf); err != nil {
			log.Fatalln(err)
		}
	}

	if _, err := w.Write(buf.Bytes()); err != nil {
		return nil, err
	}

	return &FramebufferUpdateMessage{}, nil
}
