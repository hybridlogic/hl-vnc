package main

import (
	"image"
	"log"
	"sync"
)

func imageDiffs(old, new *image.RGBA) []*Rectangle {
	// safety checks
	//if old.Rect.Min.X == new.Rect.Min.X && old.Rect.Max.X == new.Rect.Max.X && old.Rect.Min.Y == new.Rect.Min.Y && old.Rect.Max.Y == new.Rect.Max.Y {
	if old.Rect.Min != new.Rect.Min && old.Rect.Max != new.Rect.Max {
		log.Fatal("images are not the same size", old.Rect.Min, new.Rect.Min, old.Rect.Max, new.Rect.Max)
	}

	allRectangles := []*Rectangle{}

	boxSize := 64
	//Xboxes := old.Rect.Max.X / boxSize
	//Yboxes := old.Rect.Max.Y / boxSize

	//log.Println(Xboxes, old.Rect.Max)
	//log.Println(Yboxes, old.Rect.Max.Y)

	var wait sync.WaitGroup
	var lock sync.Mutex

	for y := 0; y < old.Rect.Max.Y; y = y + boxSize {
		for x := 0; x < old.Rect.Max.X; x = x + boxSize {
			//log.Println(y, x)
			//newBox := old.SubImage(image.Rect(x, y, x+boxSize-1, y+boxSize-1))
			//oldBox := new.SubImage(image.Rect(x, y, x+boxSize-1, y+boxSize-1))
			//rect := Rectangle{X: uint16(x), Y: uint16(y), Height: uint16(boxSize), Width: uint16(boxSize), Image: newBox.(*image.RGBA)}
			//if x < 200 && y < 200 {
			//	allRectangles = append(allRectangles, &rect)
			//}

			//if x < 200 && y < 200 {
			wait.Add(1)

			go func(x, y int) {
				newBox := new.SubImage(image.Rect(x, y, x+boxSize-1, y+boxSize-1))
				oldBox := old.SubImage(image.Rect(x, y, x+boxSize-1, y+boxSize-1))

				n := false
				//for y1 := newBox.Bounds().Min.Y; y1 <= newBox.Bounds().Max.Y; y1++ {
				//	for x1 := newBox.Bounds().Min.X; x1 <= newBox.Bounds().Max.X; x1++ {
				//		if oldBox.At(x1, y1) != newBox.At(x1, y1) {
				//			//old.Set(x1, y1, color.RGBA{0, 255, 0, 0})
				//			n = true
				//			break
				//		}
				//	}
				//	if n {
				//		break
				//	}
				//}
				for x1 := newBox.Bounds().Min.X; x1 <= newBox.Bounds().Max.X; x1++ {
					if oldBox.At(x1, newBox.Bounds().Min.Y) != newBox.At(x1, newBox.Bounds().Min.Y) ||
						oldBox.At(x1, newBox.Bounds().Max.Y) != newBox.At(x1, newBox.Bounds().Max.Y) {
						//old.Set(x1, y1, color.RGBA{0, 255, 0, 0})
						n = true
						break
					}
				}
				if !n {
					for y1 := newBox.Bounds().Min.Y; y1 <= newBox.Bounds().Max.Y; y1++ {
						if oldBox.At(newBox.Bounds().Min.X, y1) != newBox.At(newBox.Bounds().Min.X, y1) ||
							oldBox.At(newBox.Bounds().Max.X, y1) != newBox.At(newBox.Bounds().Max.X, y1) {
							//old.Set(x1, y1, color.RGBA{0, 255, 0, 0})
							n = true
							break
						}
					}
				}

				if n {
					log.Println("They are not equal")
					rect := Rectangle{X: uint16(x), Y: uint16(y), Height: uint16(boxSize), Width: uint16(boxSize), Image: newBox.(*image.RGBA)}
					lock.Lock()
					allRectangles = append(allRectangles, &rect)
					lock.Unlock()
				}
				wait.Done()
			}(x, y)

			//}
		}
	}
	log.Println("waiting")
	wait.Wait()

	//for y := 0; y < old.Rect.Max.Y; y++ {
	//	for x := 0; x < old.Rect.Max.X; x++ {
	//		if old.RGBAAt(x, y) != new.RGBAAt(x, y) {
	//			old.Set(x, y, color.RGBA{0, 255, 0, 0})
	//		}
	//	}
	//}
	//box1 := old.SubImage(image.Rect(0, 0, 99, 99)).(*image.RGBA)
	//box2 := old.SubImage(image.Rect(100, 100, 199, 199)).(*image.RGBA)
	//allRectangles = []*Rectangle{{
	//	X:      uint16(0),
	//	Y:      uint16(0),
	//	Height: uint16(100),
	//	Width:  uint16(100),
	//	Image:  box1,
	//}, {
	//	X:      uint16(100),
	//	Y:      uint16(100),
	//	Height: uint16(100),
	//	Width:  uint16(100),
	//	Image:  box2,
	//}}

	return allRectangles
}
