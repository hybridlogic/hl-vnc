package main

import (
	"github.com/mitchellh/go-vnc"
	"io"
)

type Encoding interface {
	// The number that uniquely identifies this encoding type.
	Type() int32

	// Read reads the contents of the encoded pixel data from the reader.
	// This should return a new Encoding implementation that contains
	// the proper data.
	Read(*Rectangle, io.Reader) (Encoding, error)
}


type Pixel struct {
	R uint16
	G uint16
	B uint16
}

func (p *Pixel) Write(r *io.Reader, format vnc.PixelFormat) {

}
