package main

import (
	"sync"
	"time"
)

type timer struct {
	timers map[string]time.Time
	mutex sync.Mutex
}

func NewTimer() *timer {
	t := timer{timers: make(map[string]time.Time)}
	return &t
}

func (t* timer) Start(i string) {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	t.timers[i] = time.Now()
}

func (t* timer) End(i string) time.Duration {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	return time.Now().Sub(t.timers[i])
}
